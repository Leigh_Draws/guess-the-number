
let tries;
let randomNumber;
let input = document.getElementById("guess");

window.addEventListener('DOMContentLoaded', function(event) {
        
    startGame() 

    function startGame () {
        tries = 0;
        randomNumber = Math.floor(Math.random() * 100)+1;
        
        console.log('le chiffre à deviner est ' + randomNumber)
    }

    const form = document.getElementById("form");
    form.addEventListener('submit', submitForm)

    function submitForm(event) {
        value = form.guess.value;
        event.preventDefault();
        console.log(value);
            if (value < randomNumber) {
                plus();
                tries++;
                console.log("essais : ", tries);
                getTries()
            } else if (value > randomNumber) {
                moins();
                tries++;
                console.log("essais : ", tries);
                getTries()
            } else if (value == randomNumber) {
                alert("C'est gagné ! Le chiffre était : " + randomNumber);
                rejouer()
            }
    }

    function getTries () {
        let spanTries = document.getElementById("tries");
        spanTries.innerText = tries;
    }
    
    function plus () {
        let messagePlus = document.getElementById("plusmoins");
        messagePlus.innerText = "C'est plus !";
        input.value = ""
    }

    function moins () {
        let messageMoins = document.getElementById("plusmoins");
        messageMoins.innerText = "C'est moins !";
        input.value = ""
    }

    function rejouer () {
        let rejouer = document.getElementById("rejouer");
        rejouer.addEventListener("click", (event) => startGame());
        input.value = ""       
    }

})












